# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=bzip2
pkgver=1.0.8
pkgrel=2
pkgdesc="A high-quality data compression program"
arch=('x86_64')
url="https://sourceware.org/bzip2"
license=('BSD')
groups=('base')
depends=('glibc' 'bash')
source=(https://www.sourceware.org/pub/${pkgname}/${pkgname}-${pkgver}.tar.gz
	${pkgname}-${pkgver}-install_docs-1.patch
	bzip2.pc)
sha256sums=(ab5a03176ee106d3f0fa90e381da478ddae405918153cca248e682cd0c4a2269
	35e3bbd9642af51fef2a8a83afba040d272da42d7e3a251d8e43255a7b496702
	862533d0df4367c9d52bef13c94d56d1aaa96b1caa1034dd4354bbf4a4822fcf)

prepare() {
	cd ${pkgname}-${pkgver}

	patch -Np1 -i ${srcdir}/${pkgname}-${pkgver}-install_docs-1.patch

	cp ${srcdir}/bzip2.pc bzip2.pc
	sed "s|@VERSION@|${pkgver}|" -i bzip2.pc

	sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
	sed -i "s@(PREFIX)/lib@(PREFIX)/lib64@g" Makefile
	sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
}

build() {
	cd ${pkgname}-${pkgver}

	make -f Makefile-libbz2_so CC="gcc ${CFLAGS} ${CPPFLAGS} ${LDFLAGS}"
	make bzip2 bzip2recover CC="gcc ${CFLAGS} ${CPPFLAGS} ${LDFLAGS}"

}

package() {
	cd ${pkgname}-${pkgver}

	make PREFIX=${pkgdir}/usr install

	cp -av libbz2.so.* ${pkgdir}/usr/lib64
	ln -sv libbz2.so.${pkgver} ${pkgdir}/usr/lib64/libbz2.so

	install -m755 bzip2-shared ${pkgdir}/usr/bin/bzip2
	install -m755 bzip2recover bzdiff bzgrep bzmore ${pkgdir}/usr/bin
	
	for i in ${pkgdir}/usr/bin/{bzcat,bunzip2}; do
		ln -sfv bzip2 ${i}
	done

	install -Dm644 bzip2.pc -t ${pkgdir}/usr/lib64/pkgconfig
}
